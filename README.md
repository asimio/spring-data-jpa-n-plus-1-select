# README #

Accompanying source code for blog entry at https://tech.asimio.net/2020/11/06/Preventing-N-plus-1-select-problem-using-Spring-Data-JPA-EntityGraph.html


### Requirements ###

* Java 8+
* Maven 3.2.x+

### Building the artifact ###

```
mvn clean package
```

### Running the application from command line ###

```
mvn spring-boot:run
```

### Available URLs

```
curl -v "http://localhost:8080/api/films"
curl -v "http://localhost:8080/api/films?page=3&size=10"
curl -v "http://localhost:8080/api/films/{id}"   (1, for instance)
```
should result in successful responses. Please look at the logs to verify the N+1 SELECT problem is not found.

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero