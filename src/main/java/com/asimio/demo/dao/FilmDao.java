package com.asimio.demo.dao;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asimio.demo.domain.Film;

@Repository
public interface FilmDao extends JpaRepository<Film, Integer> {

    // Fixes N+1 queries but causes another problem:
    // QueryTranslatorImpl   : HHH000104: firstResult/maxResults specified with collection fetch; applying in memory!
    // Fixed at https://tech.asimio.net/2021/05/19/Fixing-Hibernate-HHH000104-firstResult-maxResults-warning-using-Spring-Data-JPA.html
    @EntityGraph(
            type = EntityGraphType.FETCH,
            attributePaths = { 
                    "language", 
                    "filmActors",
                    "filmActors.actor"
            }
    )
    Page<Film> findAll(Pageable pageable);

    @EntityGraph(
            type = EntityGraphType.FETCH,
            attributePaths = { 
                    "language", 
                    "filmActors",
                    "filmActors.actor"
            }
    )
    Optional<Film> findById(Integer id);
}